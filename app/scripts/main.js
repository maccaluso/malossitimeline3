(function() {
	'use strict';

	var tlData, filteredData, fullData, seq;
	var filtersArray = [];
	var colorsArray = [];
	var activeFilters = [];
	var seqOptions = {
		keyNavigation: false,
		startingStepAnimatesIn: true,
		animateCanvasDuration: 1000,
		preloader: true,
		hashTags: true,
		hashDataAttribute: true
	}
	var galleryInterval, mixedInterval;
	var currentSlide;
	mapboxgl.accessToken = 'pk.eyJ1IjoibWFjY2FsdXNvIiwiYSI6ImNpcHp2NTRjdjAwYnRoeG5vZTdxbGVxM2cifQ.45e6iKKilwaQQHMLEYIiPA';
	var mapboxglStaticAPIURL = 'https://api.mapbox.com/styles/v1/maccaluso/ciqr0vutw004uc8j50vitszoj/static/';

	// var config = {
	// 	apiKey: 'AIzaSyDMq7khZxnfYkSfTNrxjm1heqYxwyb85Ak',
	// 	authDomain: 'malossi-timeline-3.firebaseapp.com',
	// 	databaseURL: 'https://malossi-timeline-3.firebaseio.com',
	// 	storageBucket: 'malossi-timeline-3.appspot.com',
	// 	messagingSenderId: '703006344448'
	// };
	// var config = {
	// 	apiKey: 'AIzaSyBXwnimrairRFx_0nOMqKqF4oG_heg9kq0',
	// 	authDomain: 'malossi-timeline-54247.firebaseapp.com',
	// 	databaseURL: 'https://malossi-timeline-54247.firebaseio.com',
	// 	storageBucket: 'malossi-timeline-54247.appspot.com',
	// 	messagingSenderId: '338293992834'
	// };
	var config = {
		apiKey: 'AIzaSyCmoECZJ01owsFld9FJINfQwmAZV7Z8kbE',
		authDomain: 'malossi-timeline-d9dee.firebaseapp.com',
		databaseURL: 'https://malossi-timeline-d9dee.firebaseio.com',
		projectId: 'malossi-timeline-d9dee',
		storageBucket: 'malossi-timeline-d9dee.appspot.com',
		messagingSenderId: '627018571896'
	};
	firebase.initializeApp(config);

	var database = firebase.database().ref('decades');
	database.once('value').then(function(snapshot){
		tlData = snapshot.val();
		fullData = JSON.parse(JSON.stringify(tlData));

		buildLayout();
		buildFilters();
		initVideoListeners();
		initTimeline();
		initUI();
	});

	function buildLayout() {
		$('#timelineCanvas, #paginationContainer').html('');
		$('.seq-preloader').remove();

		var counter = 0;

		for( var i in tlData )
		{
			// console.log('Decade: ' + i);

			if(tlData[i].years)
			{
				var decadeString = 'Anni \'' + i.slice(-2);
				if( i == 5000 ) { decadeString = ' &bull;&bull;&bull; '; }

				var decadeLink = '';
				decadeLink += '<li class="decade-link">';
					decadeLink += decadeString;
					decadeLink += '<ul class="decade-ul" id="decadeUl' + i + '" data-decade="' + i + '">';
					decadeLink += '</ul>';
				decadeLink += '</li>';

				$('#paginationContainer').append( decadeLink );

				var years = tlData[i].years;

				for( var j in years )
				{
					// console.log('-- Anno: ' + j)

					if( years[j].group && years[j].media && activeFilters.indexOf(years[j].group.type) == -1 )
					{
						var yearData = years[j];
						// var paginationString = j;
						var paginationString = yearData.paginationLabel;

						// console.log(years[j].year)

						if( yearData.group.type != 'contacts' ) 
						{
							buildStep( yearData, i ); 
						}
						else
						{
							var paginationString = '&hearts;';
							buildContacts( yearData, i );
						}

						var pLink = '';
						pLink += '<li ';
							pLink += 'class="pagination-link" ';
							pLink += 'data-year="' + years[j].year + '" ';
							pLink += 'data-step="' + counter + '" ';
							pLink += 'data-color="' + yearData.group.backgroundColor + '" ';
							pLink += 'data-linktype="' + yearData.group.type + '">';
							pLink += paginationString;
						pLink += '</li>';
						// $('#decadeUl' + i).append('<li class="pagination-link" data-year="' + years[j].year + '" data-color="' + yearData.group.backgroundColor + '" data-linktype="' + yearData.group.type + '">' + paginationString + '</li>')
						$('#decadeUl' + i).append( pLink );

						counter++;
					}
					else
					{
						// console.log('---- no data')
					}
				}
			}
		}

		filtersArray = unique(filtersArray);
		colorsArray = unique(colorsArray);
	}
	function buildStep( stepData, index ) {

		var step = $( '<li style="background-color: ' + stepData.group.backgroundColor + ';"></li>' )
			.addClass( 'timeline-step' )
			.attr( 'id', 'step' + stepData.year )
			.attr( 'data-steptype', stepData.group.type )
			.attr( 'data-seq-hashtag', slug( stepData.headline, {lower: true} ) )
			.attr( 'data-decade', index )
			.attr( 'data-year', stepData.year )
			.attr( 'data-color', stepData.group.backgroundColor );

		step.append(
			'<div class="content">' + 
				'<h2>' + stepData.year + '</h2>' +
				'<h3>' + stepData.headline + '</h3>' +
				'<p>' + stepData.text + '</p>' +
			'</div>'
		);

		if( stepData.position )
		{
			step
			.find('.content').css({
				top: stepData.position.top,
				left: stepData.position.left,
				bottom: stepData.position.bottom,
				right: stepData.position.right,
				verticalAlign: 'initial',
				display: 'block',
				position: 'absolute',
				margin: 0,
				minWidth: 'initial'
			})
			.find('p').css({
				margin: 0
			});
		}

		if( stepData.style )
		{
			step
			.find('.content').css({
				textAlign: stepData.style.align,
				backgroundColor: stepData.style.backgroundColor,
				opacity: stepData.style.opacity,
				padding: stepData.style.padding
			})
			.find('h2').css({
				color: stepData.style.dateColor,
				backgroundColor: stepData.style.dateBackgroundColor,
				opacity: stepData.style.dateOpacity,
				padding: stepData.style.datePadding
			})
			.next('h3').css({
				color: stepData.style.headlineColor,
				backgroundColor: stepData.style.headlineBackgroundColor,
				opacity: stepData.style.headlineOpacity,
				padding: stepData.style.headlinePadding
			})
			.next('p').css({
				color: stepData.style.textColor,
				backgroundColor: stepData.style.textBackgroundColor,
				opacity: stepData.style.textOpacity,
				padding: stepData.style.textPadding
			});
		}

		if( stepData.media )
		{
			switch( stepData.media.type ){
				case 'empty':
					step.addClass('empty-step');

					var emptyBG = $('<div class="emptyBG"></div>');
					step.prepend( emptyBG );

					$('#timelineCanvas').append( step );

					break;
				case 'img':
					step.addClass('image-step');

					var imageBG = $('<div class="imageBG"></div>');
					imageBG.css({ backgroundImage: 'url(' + stepData.media.url + ')' });
					
					var imageCaption = $('<p class="caption"></p>');
					imageCaption.text(stepData.media.caption.text);
					imageCaption.css({
						top: stepData.media.caption.position.top,
						left: stepData.media.caption.position.left,
						bottom: stepData.media.caption.position.bottom,
						right: stepData.media.caption.position.right,
						color: stepData.media.caption.color,
						backgroundColor: stepData.media.caption.backgroundColor
					});

					step.prepend( imageCaption );
					step.prepend( imageBG );

					$('#timelineCanvas').append( step );
					
					break;
				case 'gallery':
					step.addClass('gallery-step');

					var gallerySlide = $('<div class="gallerySlide"></div>');
					for(var i=0; i<stepData.media.images.length; i++){
						
						var image = stepData.media.images[i];

						var imgContainer = $('<div class="img-container" style="background-image: url(' + image.url + ');"></div>');
						
						var imageCaption = $('<p class="caption"></p>');
						imageCaption.text( image.caption.text );
						if( image.caption.position != undefined )
						{
							imageCaption.css({
								top: image.caption.position.top != undefined ? image.caption.position.top : 'auto',
								left: image.caption.position.left,
								bottom: image.caption.position.bottom,
								right: image.caption.position.right
							});
						}
						imageCaption.css({
							color: image.caption.color,
							backgroundColor: image.caption.backgroundColor,
							padding: image.caption.padding
						});

						imgContainer.append(imageCaption);

						gallerySlide.append(imgContainer);
					}
					step.prepend( gallerySlide );

					$('#timelineCanvas').append( step );

					break;
				case 'map':
					step.addClass('map-step');

					var mapContainer = $('<div id="map' + index + '" class="map-container"></div>');
					step.prepend( mapContainer );

					$('#timelineCanvas').append( step );

					var mapCenterCoords = new mapboxgl.LngLat( stepData.media.mapCenter.lng, stepData.media.mapCenter.lat );

					var map = new mapboxgl.Map({
						container: 'map' + index,
						style: stepData.media.mapStyle,
						center: mapCenterCoords,
						zoom: stepData.media.zoom,
					});
					map.getCanvas().style.left = 0;

					var markersData = stepData.media.markers;
					markersData.data.features.forEach(function(marker) {
						
						var style = marker.properties.markerStyle;
						
						var el = document.createElement('div');
						
						var svg = '';
						svg += '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0" y="0" width="35" height="50" viewBox="0, 0, 35, 50">'
							svg += '<g id="Livello_1">';
								svg += '<path d="M17.5,0 C7.836,0 0,7.793 0,17.405 C0,30.314 17.5,50 17.5,50 C17.5,50 35,30.314 35,17.405 C35,7.793 27.164,0 17.5,0 z M17.5,24.748 C13.421,24.748 10.116,21.461 10.116,17.405 C10.116,13.348 13.421,10.061 17.5,10.061 C21.578,10.061 24.884,13.348 24.884,17.405 C24.884,21.461 21.578,24.748 17.5,24.748 z" ';
								svg += 'fill="' + style.markerColor + '" stroke="' + style.markerOutlineColor + '" stroke-width="' + style.markerOutlineWidth + '"';
								svg += '/>';
							svg +='</g>';
						svg += '</svg>';

						var span = document.createElement('span');
						span.innerHTML = marker.properties.title;
						span.style.color = style.textColor;
						span.style.textShadow = '-' + style.textOutlineWidth + 'px -' + style.textOutlineWidth + 'px 0 ' 
						+ style.textOutlineColor + ',' + style.textOutlineWidth + 'px -' + style.textOutlineWidth + 'px 0 ' 
						+ style.textOutlineColor + ',-' + style.textOutlineWidth + 'px ' + style.textOutlineWidth + 'px 0 ' 
						+ style.textOutlineColor + ',' + style.textOutlineWidth + 'px ' + style.textOutlineWidth + 'px 0 ' 
						+ style.textOutlineColor;

						el.innerHTML = svg;
						el.appendChild( span );
						el.className = 'marker';

						new mapboxgl.Marker(el)
						.setLngLat(marker.geometry.coordinates)
						.addTo(map);
					});

					break;
				case 'video':
					step.addClass( 'video-step' );

					var videoBG = $('<video class="videoBG hide-out"></video>');
					var sources = [
						$('<source src="' + stepData.media.mp4Preview + '" type="video/mp4"></source>'),
						$('<source src="' + stepData.media.webmPreview + '" type="video/webm"></source>'),
						$('<source src="' + stepData.media.ogvPreview + '" type="video/ogv"></source>')
					];
					// videoBG.attr('src', stepData.media.preview);
					videoBG.append( sources[0] ).append( sources[1] ).append( sources[2] );
					step.prepend(videoBG);

					var playBtn = $( '<button>watch movie</button>');
					step.find('div.content').append( playBtn );

					playBtn.on('click', function(e){
						hideInterface();
						$('#play').hide();
						$('#pause').show();
						showVideoControls();
						step.find('.videoBG')[0].play();
					});

					$('#timelineCanvas').append( step );

					break;
				case 'videoBG':
					step.addClass( 'video-step' );

					// var videoBG = $('<video autoplay controls poster="' + stepData.media.poster + '" class="videoBG primaryVideoBG"></video>');
					var videoBG = $('<video autoplay class="videoBG primaryVideoBG"></video>');
					var sources = [
						$('<source src="' + stepData.media.mp4Preview + '" type="video/mp4"></source>'),
						$('<source src="' + stepData.media.webmPreview + '" type="video/webm"></source>'),
						$('<source src="' + stepData.media.ogvPreview + '" type="video/ogv"></source>')
					];
					
					if(stepData.media.loop)
						videoBG.prop('loop', true);

					videoBG.append( sources[0] ).append( sources[1] ).append( sources[2] );
					step.prepend(videoBG);

					$('#timelineCanvas').append( step );

					break;
				case 'mixed':
					step.addClass('mixed-step');

					var mixedSlide = $('<div class="mixedSlide"></div>');
					step.prepend( mixedSlide );

					$('#timelineCanvas').append( step );
					
					var maps = [];
					for(var i=0; i<stepData.media.sequence.length; i++){
						
						var mixedContainer;
						var slideData = stepData.media.sequence[i];
						
						if( slideData.type == 'img' )
						{
							mixedContainer = $('<div class="mixed-container" style="background-image: url(' + slideData.url + ');"></div>');
							mixedSlide.append( mixedContainer );
						}
						if( slideData.type == 'map' )
						{
							maps.push(slideData);
						}
					}

					if(maps.length > 0) {
						var mapThumbsContainer = $('<div class="map-thumbs-container"></div>');

						for(var i=0; i<maps.length; i++){
							
							var slideData = maps[i];

							mixedContainer = $('<div id="mixedMap' + i + '" class="map-mixed-container"></div>');
							mixedSlide.append( mixedContainer );

							var mapCenterCoords = new mapboxgl.LngLat( slideData.mapCenter.lng, slideData.mapCenter.lat );

							var map = new mapboxgl.Map({
								container: mixedContainer[0],
								style: slideData.mapStyle,
								center: mapCenterCoords,
								zoom: slideData.zoom
							});
							map.getCanvas().style.left = 0;

							var markersData = slideData.markers;
							markersData.data.features.forEach(function(marker) {
								var style = marker.properties.markerStyle;
						
								var el = document.createElement('div');
								
								var svg = '';
								svg += '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0" y="0" width="35" height="50" viewBox="0, 0, 35, 50">'
									svg += '<g id="Livello_1">';
										svg += '<path d="M17.5,0 C7.836,0 0,7.793 0,17.405 C0,30.314 17.5,50 17.5,50 C17.5,50 35,30.314 35,17.405 C35,7.793 27.164,0 17.5,0 z M17.5,24.748 C13.421,24.748 10.116,21.461 10.116,17.405 C10.116,13.348 13.421,10.061 17.5,10.061 C21.578,10.061 24.884,13.348 24.884,17.405 C24.884,21.461 21.578,24.748 17.5,24.748 z" ';
										svg += 'fill="' + style.markerColor + '" stroke="' + style.markerOutlineColor + '" stroke-width="' + style.markerOutlineWidth + '"';
										svg += '/>';
									svg +='</g>';
								svg += '</svg>';

								var span = document.createElement('span');
								span.innerHTML = marker.properties.title;
								span.style.color = style.textColor;
								span.style.textShadow = '-' + style.textOutlineWidth + 'px -' + style.textOutlineWidth + 'px 0 ' 
								+ style.textOutlineColor + ',' + style.textOutlineWidth + 'px -' + style.textOutlineWidth + 'px 0 ' 
								+ style.textOutlineColor + ',-' + style.textOutlineWidth + 'px ' + style.textOutlineWidth + 'px 0 ' 
								+ style.textOutlineColor + ',' + style.textOutlineWidth + 'px ' + style.textOutlineWidth + 'px 0 ' 
								+ style.textOutlineColor;

								el.innerHTML = svg;
								el.appendChild( span );
								el.className = 'marker';

								new mapboxgl.Marker(el)
								.setLngLat(marker.geometry.coordinates)
								.addTo(map);
							});

							var staticMap = mapboxglStaticAPIURL 
											+ slideData.mapCenter.lng
											+ ','
											+ slideData.mapCenter.lat
											+ ','
											+ '12'
											+ '/'
											+ '100x100@2x?access_token='
											+ mapboxgl.accessToken;
							
							var mapThumb = $('<div id="mapThumb' + i + '" class="map-thumb"><div class="map-thumb-hover"></div></div>');
							var img = $('<img>');
							img.attr('src',staticMap);

							mapThumb.append( img );
							mapThumbsContainer.append( mapThumb );
						}

						if(stepData.media.autoPlay) { mapThumbsContainer.addClass('gallery-autoplay') }
						step.append( mapThumbsContainer );
					}

					break;
			}
		}

		filtersArray.push( stepData.group.type );
		colorsArray.push( stepData.group.backgroundColor );
	}
	function buildContacts( stepData, index ) {
		var contactsStep =  $( '<li style="background-color: ' + stepData.group.backgroundColor + ';"></li>' )
			.addClass( 'timeline-step' )
			.attr( 'data-steptype', stepData.group )
			.attr( 'data-seq-hashtag', slug( stepData.headline, {lower: true} ) )
			.attr( 'data-decade', index )
			.attr( 'data-year', stepData.year )
			.attr( 'data-color', stepData.group.backgroundColor );

		contactsStep.append(
			'<div class="content">' + 
				'<h3>' + stepData.headline + '</h3>' +
				'<p>' + stepData.text + '</p>' +
				'<ul></ul>' +
			'</div>'
		);
		for( var i in stepData.links )
		{
			contactsStep.find('ul').append(
				'<li><a target="_blank" href="' + stepData.links[i].url + '">' + stepData.links[i].label + '</a></li>'
			);
		}

		$('#timelineCanvas').append( contactsStep );

		// var paginationLink = $('<li> &bull;&bull;&bull; </li>');
		// $('#paginationContainer').append( paginationLink );
	}
	function buildFilters() {
		for( var i = 0; i < filtersArray.length; i++ )
		{
			var html = '';
			html += '<a class="filter-link ' + filtersArray[i] + '-link" href="#" style="background-color: ' + colorsArray[i] + '" data-type="' + filtersArray[i] + '">';
				html += '<span class="remove-filter" style="color: ' + colorsArray[i] + '">';
					html += '<svg height="14" width="14">';
						html += '<circle class="outer-circle" cx="7" cy="7" r="7" stroke="white" stroke-width="1" fill="transparent" />'
						html += '<circle class="inner-circle on" cx="7" cy="7" r="5" fill="white" />'
					html += '</svg>';
				html += '</span>';
				html += filtersArray[i];
			html += '</a>';

			if( $('#filtersContainer').find( '.' + filtersArray[i] + '-link' ).length == 0 )
				$('#filtersContainer').append(html);
		}
	}

	function hideInterface(){ $('.content, #paginationContainer').hide(); }
	function showInterface(){ $('.content, #paginationContainer').show(); }

	function showVideoControls(){ $('#videoControls').show(); }
	function hideVideoControls(){ $('#videoControls').hide(); }

	function showGalleryControls(){ $('#galleryControls').show(); }
	function hideGalleryControls(){ $('#galleryControls').hide(); }

	function initVideoListeners(){
		
		// Video Controls
		$('#closeVideoPlayer').on('click', function(e){
			$('.videoBG').each(function(i){
				$(this)[0].pause();
				$(this)[0].currentTime = 0;
			});
			initTimeline();
			hideVideoControls();
			showInterface();
		});

		$('#play').on('click', function(e){ 
			var currentVideo = $('.timeline-step').eq( seq.currentStepId - 1 ).find('.videoBG');
			currentVideo[0].play();

			$(this).hide();
			$('#pause').show();
		});
		$('#pause').on('click', function(e){ 
			var currentVideo = $('.timeline-step').eq( seq.currentStepId - 1 ).find('.videoBG');
			currentVideo[0].pause();
			
			$(this).hide();
			$('#play').show();
		});

		$('#toggleFullScreen').on('click', function(e){
			// if( document.fullscreenEnabled || document.msfullscreenEnabled || document.mozfullscreenEnabled || document.webkitFullscreenEnabled )
			// {
			// 	if ($('.videoBG')[0].requestFullscreen) {
			// 		$('.videoBG')[0].requestFullscreen();
			// 	} else if ($('.videoBG')[0].webkitRequestFullscreen) {
			// 		$('.videoBG')[0].webkitRequestFullscreen();
			// 	} else if ($('.videoBG')[0].mozRequestFullScreen) {
			// 		$('.videoBG')[0].mozRequestFullScreen();
			// 	} else if ($('.videoBG')[0].msRequestFullscreen) {
			// 		$('.videoBG')[0].msRequestFullscreen();
			// 	}
			// }
		});

		// Video Events
		// $('.videoBG').on('canplay', function(e){
		// 	console.log('video canplay', e.currentTarget);
		// });

		$('.videoBG').on('timeupdate', function(e){
			if( !$(this).hasClass('primaryVideoBG') )
			{
				var percentage = (100 / e.target.duration) * e.target.currentTime;
				$('#progressCursor').css({ width: percentage + '%' });
			}
		});

		$('.videoBG').on('ended', function(e){
			console.log('video ended');
			$('#pause').hide();
			$('#play').show();
			$('#closeVideoPlayer').trigger('click');

			if( $(this).hasClass('primaryVideoBG') )
			{
				this.autoplay = false;
				this.currentTime = this.duration - 0.1;
				// this.load();
			}
		});
	}

	function initTimeline() {
		seq = sequence( $('#timeline')[0], seqOptions );

		seq.ready = function() { console.log( 'ready' ); }
		seq.destroyed = function() { 
			console.log( 'destroyed' ); 
			buildLayout();
			initTimeline();
		}

		seq.animationStarted = function( id, sequence ) {
			// console.log('animation started');

			clearInterval( galleryInterval );
			clearInterval( mixedInterval );
			$('#closeVideoPlayer').trigger('click');
			hideGalleryControls();
			$('.videoBG, .gallerySlide, .map-container, .imageBG').addClass('hide-out');
		}

		seq.animationEnded = function( id, sequence ){
			// console.log( 'animation ended' );

			if( $(seq.$steps[ id - 1 ]).hasClass('video-step') ) 
			{
				$(seq.$steps[ id - 1 ]).find('.videoBG')[0].play();
				$(seq.$steps[ id - 1 ]).find('.videoBG').removeClass('hide-out');
			}

			if( $(seq.$steps[ id - 1 ]).hasClass('gallery-step') )
			{
				var decade = $(seq.$steps[ id - 1 ]).data('decade') + '';
				var year = $(seq.$steps[ id - 1 ]).data('year') + '';

				var galleryData = tlData[decade].years[year];

				var sequence = galleryData.media.images;
				var tick = sequence.length;
				var slides = $(seq.$steps[ id - 1 ]).find('.gallerySlide').find('.img-container');

				if(galleryData.media.autoPlay)
				{
					clearInterval( galleryInterval );
					clearInterval( mixedInterval );
					galleryInterval = setInterval( function(){
						tick--;
						slides.eq(tick).addClass('fade-out');
						if( tick == 0 ) { 
							// console.log( 'minore di zero' )
							slides.removeClass('fade-out');
							slides.eq(tick).removeClass('fade-out');
							tick = slides.length; 
						}
					}, 2500 );
				}
				else
				{
					$('body').on('click', '.slide-control', function(e){
						
						if( $(this).attr('id') == 'nextSlide' ) {
							tick--;
							slides.eq(tick).addClass('fade-out');
							if( tick == 0 ) { 
								// console.log( 'minore di zero' )
								slides.removeClass('fade-out');
								slides.eq(tick).removeClass('fade-out');
								tick = slides.length; 
							}
						}
						else
						{
							if( tick > slides.length - 1 ) { 
								// console.log( 'maggiore del count - 1' )
								tick = 0; 
								slides.addClass('fade-out');
								slides.eq(tick).removeClass('fade-out');
							}
							slides.eq(tick).removeClass('fade-out');
							tick++;
						}
					});

					showGalleryControls();
				}

				$('.gallerySlide').removeClass('hide-out');
			}

			if( $(seq.$steps[ id - 1 ]).hasClass('mixed-step') ) 
			{
				var decade = $(seq.$steps[ id - 1 ]).data('decade') + '';
				var year = $(seq.$steps[ id - 1 ]).data('year') + '';
				var galleryData = tlData[decade].years[year];

				var sequence = galleryData.media.sequence;
				var tick = 0;
				for(var i=0; i<sequence.length; i++)
				{
					if(sequence[i].type != 'map') { tick++; }
				}
				var slides = $(seq.$steps[ id - 1 ]).find('.mixedSlide').find('.mixed-container');

				if(galleryData.media.autoPlay)
				{
					clearInterval( galleryInterval );
					clearInterval( mixedInterval );
					mixedInterval = setInterval( function(){
						tick--;
						slides.eq(tick).addClass('fade-out');
						if( tick == 0 ) { 
							// console.log( 'minore di zero' )
							slides.removeClass('fade-out');
							slides.eq(tick).removeClass('fade-out');
							tick = slides.length; 
						}
					}, galleryData.media.delay );
				}
				else
				{
					$('body').on('click', '.slide-control', function(e){

						if( $(this).attr('id') == 'nextSlide' ) {
							tick--;
							slides.eq(tick).addClass('fade-out');
							if( tick == 0 ) { 
								// console.log( 'minore di zero' )
								slides.removeClass('fade-out');
								slides.eq(tick).removeClass('fade-out');
								tick = slides.length; 
							}
						}
						else
						{
							if( tick > slides.length - 1 ) { 
								// console.log( 'maggiore del count - 1' )
								tick = 0; 
								slides.addClass('fade-out');
								slides.eq(tick).removeClass('fade-out');
							}
							slides.eq(tick).removeClass('fade-out');
							tick++;
						}

						slides.css({ pointerEvents: 'auto' });
						if( slides.eq(tick-1).hasClass('map-mixed-container') )
						{
							slides.css({ pointerEvents: 'none' });
							slides.eq(tick-1).css({ pointerEvents: 'auto' });
						}
					});

					showGalleryControls();
				}

				$('.mixedSlide').removeClass('hide-out');
			}

			if( $(seq.$steps[ id - 1 ]).hasClass('map-step') ) 
			{

				$('.map-container').removeClass('hide-out');
			}

			if( $(seq.$steps[ id - 1 ]).hasClass('image-step') ) 
			{

				$('.imageBG').removeClass('hide-out');
			}

			$('.decade-ul').removeClass('expanded');
			var decade = $(seq.$steps[ id - 1 ]).data().decade;
			$('*[data-decade="' + decade + '"]').not('.timeline-step').addClass('expanded');

			$('.pagination-link').removeClass('seq-current').css({ color: '#3d3d3d' });
			var year = $(seq.$steps[ id - 1 ]).data().year;
			
			var navigationColor = $(seq.$steps[ id - 1 ]).data().color;
			if( tlData[decade].years[year].group.navigationColor ) { navigationColor = tlData[decade].years[year].group.navigationColor; }
			// $('*[data-year="' + year + '"]').not('.timeline-step').addClass('seq-current').css({ color: navigationColor });
			$('*[data-step="' + (id-1) + '"]').not('.timeline-step').addClass('seq-current').css({ color: navigationColor });

			
			var navigationBackground = 'rgba(255,255,255,0)';
			if( tlData[decade].years[year].group.navigationBackground ) { navigationBackground = tlData[decade].years[year].group.navigationBackground; }
			$('#paginationContainer').css({ backgroundColor: navigationBackground });

			$('#svgLogo').css({ fill: $(seq.$steps[ id - 1 ]).data().color });
			
			var footerColor = '#ffffff';
			if( tlData[decade].years[year].group.footerColor ) { footerColor = tlData[decade].years[year].group.footerColor }
			$('#footer').css({ backgroundColor: $(seq.$steps[ id - 1 ]).data().color, color: footerColor });
		}
	}

	function initUI() {
		$('body').on('click', '.map-thumb', function(e){
			var thumbID = $(this).attr('id').replace('mapThumb','');
			var mapSelector = '#mixedMap' + thumbID;

			var map = $(this).parent().parent().find('.mixedSlide').find(mapSelector);
			var hover = $(this).find('.map-thumb-hover');
			
			if(map.hasClass('visible'))
			{
				map.removeClass('visible');
				hover.removeClass('hide');
				if( !$(this).parent().hasClass('gallery-autoplay') ) { showGalleryControls(); }
				showInterface();
			}
			else
			{
				map.addClass('visible');
				hover.addClass('hide');
				hideGalleryControls();
				hideInterface();
			}
		});

		$('body').on('click', '.pagination-link', function(e){
			var y = $(this).data('year');
			var targetIndex = $('*[data-year="' + y + '"]').index() + 1;
			console.log(targetIndex);
			targetIndex = $(this).data('step') + 1;
			seq.goTo(targetIndex,1);
		});

		$('body').on('click', '.decade-link', function(e){
			if( $(e.target).hasClass('pagination-link') )
			{
				
			}
			else
			{
				$(this).find('ul').find('li').first().trigger('click');
			}
		});

		$('body').on('click', '.filter-link', function(e){
			e.preventDefault();

			var type = $(this).data().type;
			var innerCircle = $(this).find('.inner-circle');

			if( innerCircle.attr('class').indexOf('on') > -1 )
			{
				// console.log('filtra');

				if( $('.inner-circle').length - $('.inner-circle.on').length == $('.inner-circle').length - 1 )
				{
					return;
				}
				// console.log( $('.inner-circle').length - $('.inner-circle.on').length == 2 );

				innerCircle.attr('class','inner-circle');

				activeFilters.push( type );

				// return;
			}
			else
			{
				// console.log('ripristina');

				innerCircle.attr('class','inner-circle on');

				var indexToPop = activeFilters.indexOf( type );
				activeFilters.splice( indexToPop, 1 );
			}

			seq.destroy();
		});

		$(window).on('keydown', function(e){
			switch(e.which) {
				case 38:
					seq.prev();
					break;
				case 40:
					seq.next();
					break;
			}
		});
	}

	function unique(array){
	    return array.filter(function(el, index, arr) {
	        return index == arr.indexOf(el);
	    });
	}

}());